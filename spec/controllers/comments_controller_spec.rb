require 'spec_helper'

describe CommentsController do
  before :each do
    @user = FactoryGirl.create(:user)
  end

  describe 'POST #create' do
    context 'with valid data' do
      it 'saves @comment in db' do
        expect {
          post :create, user_id: @user, comment: {
            body: Faker::Lorem.paragraphs.join,
            user_id: @user.id
          }
        }.to change(Comment,:count).by 1
      end
      it 'redirects to @user page' do
        post :create, user_id: @user, comment: {
          body: Faker::Lorem.paragraphs.join,
          user_id: @user.id
        }
        expect(response).to redirect_to user_path(@user)
      end
    end
    context 'with invalid data' do
      it 'does not save @comment in db' do
        expect {
          post :create, user_id: @user, comment: {
            body: nil,
            user_id: @user.id
          }
        }.to change(Comment,:count).by 0
      end
      it 'redirects to @user page' do
        post :create, user_id: @user, comment: {
          body: nil,
          user_id: @user.id
        }
        expect(response).to redirect_to user_path(@user)
      end
    end
  end
end
