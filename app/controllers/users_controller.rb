class UsersController < InheritedResources::Base
  protected
  def permitted_params
    params.permit(user: [:name])
  end

  def collection
    @users ||= end_of_association_chain.order('name asc').page(params[:page]).per(5)
  end
end
