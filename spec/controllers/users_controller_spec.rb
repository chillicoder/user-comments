require 'spec_helper'

describe UsersController do
  render_views

  describe "GET 'index'" do
    it 'returns a list of @users' do
      user = FactoryGirl.create(:user)
      get :index
      expect(assigns(:users)).to eq [user] 
    end
    it 'renders :index template' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET 'new'" do
    it 'returns a new instance of @user' do
      get :new
      expect(assigns(:user)).to be_a_new(User)
    end
    it 'renders :new template' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET 'show'" do
    let(:user) { FactoryGirl.create(:user) }
    it 'returns @user' do
      get :show, id: user
      expect(assigns(:user)).to eq user
    end
    it 'renders :show template' do
      get :show, id: user
      expect(response).to render_template :show
    end
  end

  describe "GET 'edit'" do
    let(:user) { FactoryGirl.create(:user) }
    it 'returns @user' do
      get :edit, id: user
      expect(assigns(:user)).to eq user
    end
    it 'renders :edit template' do
      get :edit, id: user
      expect(response).to render_template :edit
    end
  end

  describe 'POST #create' do
    context 'with valid data' do
      it 'saves @user in db' do
        expect {
          post :create, user: FactoryGirl.attributes_for(:user)
        }.to change(User,:count).by 1
      end
      it 'redirects to @user page' do
        post :create, user: FactoryGirl.attributes_for(:user)
        expect(response).to redirect_to user_path(assigns(:user))
      end
    end
    context 'with invalid data' do
      it 'does not save @user in db' do
        expect {
          post :create, user: FactoryGirl.attributes_for(:invalid_user)
        }.to change(User,:count).by 0
      end
      it 'renders :new template' do
        post :create, user: FactoryGirl.attributes_for(:invalid_user)
        expect(response).to render_template :new
      end
    end
  end

  describe 'PUT #update' do
    it 'returns @user' do
      user = FactoryGirl.create(:user)
      put :update, id:user, user: { name: 'Carlos' }
      expect(assigns(:user)).to eq user
    end
    context 'with valid data' do
      let(:user) { FactoryGirl.create(:user) }
      it 'update @user in db' do
        put :update, id:user, user: { name: 'Carlos' }
        user.reload
        expect(user.name).to eq 'Carlos'
      end
      it 'redirects to @user page' do
        put :update, id:user, user: { name: 'Carlos' }
        expect(response).to redirect_to user_path(user)
      end
    end
    context 'with invalid data' do
      let(:user) { FactoryGirl.create(:user) }
      it 'does not update @user in db' do
        put :update, id:user, user: { name: nil }
        user.reload
        expect(user.name).to_not be_nil
      end
      it 'renders :edit template' do
        put :update, id:user, user: { name: nil }
        expect(response).to render_template :edit
      end
    end
  end
end
