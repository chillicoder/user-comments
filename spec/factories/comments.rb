# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    body { Faker::Lorem.paragraphs.join }
    user factory: :user

    factory :invalid_comment do
      body nil
    end
  end
end
