require 'spec_helper'

describe User do
  describe 'factories' do
    it 'has a valid factory' do
      expect(FactoryGirl.build(:user)).to be_valid
    end
    it 'has an invalid factory' do
      expect(FactoryGirl.build(:invalid_user)).to_not be_valid
    end
  end

  describe 'attributes' do
    subject { FactoryGirl.build(:user) }
    it { should respond_to :name }
    it { should respond_to :created_at }
  end

  describe 'validations' do
    subject { FactoryGirl.build(:user) }
    it { should validate_presence_of :name }
  end

  describe 'associations' do
    subject { FactoryGirl.build(:user) }
    it { should have_many :comments }
  end
end
