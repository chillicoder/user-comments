source 'https://rubygems.org'
ruby '2.0.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.2'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Paginator
gem 'kaminari'
# form helper
gem 'simple_form'
# controller CRUD helper
gem 'inherited_resources'
# Rails plugin for Haml template language
gem 'haml-rails'
# Rails plugin for Bootstrap 2.x layout
gem 'bootstrap-sass', '~> 2.3'
# Rails plugin for styling kaminari paginator in views
gem 'kaminari-bootstrap'

group :development do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  # silence asset compilation in development environment
  gem 'quiet_assets'
  # layout helpers
  gem 'rails_layout'
end

group :test, :development do
  # Testing framework
  gem 'rspec-rails'
  # Factory helper
  gem 'factory_girl_rails'
  # Spec extensions
  gem 'shoulda-matchers'
end

group :test do
  # Generates fake data for factories
  gem 'ffaker'
end

group :production do
  # postgresql database adapter
  gem 'pg'
  # heroku helpers
  gem 'rails_12factor'
  # web server
  gem 'thin'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
