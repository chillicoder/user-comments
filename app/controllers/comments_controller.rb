class CommentsController < ApplicationController
  before_filter :find_user

  def create
    @comment = @user.comments.new(comment_params)
    if @comment.save
      redirect_to user_path(@user)
    else
      redirect_to user_path(@user)
    end
  end

  protected
  def comment_params
    params.require(:comment).permit(:body)
  end

  def find_user
    @user = User.find(params[:user_id])
  end
end
