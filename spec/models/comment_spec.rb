require 'spec_helper'

describe Comment do
  describe 'factories' do
    it 'has a valida factory' do
      expect(FactoryGirl.build(:comment)).to be_valid
    end
    it 'has an invalid factory' do
      expect(FactoryGirl.build(:invalid_comment)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { FactoryGirl.build(:comment) }
    it { should respond_to :body }
    it { should respond_to :created_at }
  end
  describe 'associations' do
    subject { FactoryGirl.build(:comment) }
    it { should belong_to :user }
  end
  describe 'validations' do
    subject { FactoryGirl.build(:comment) }
    it { should validate_presence_of :body }
  end
end
